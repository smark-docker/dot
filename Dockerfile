# SPDX-FileCopyrightText: 2022 Sebastian Mark
#
# SPDX-License-Identifier: CC-BY-SA-4.0

FROM alpine
MAINTAINER smark

ENV GRAPHVIZ_VERSION 5.0.0

RUN apk add --no-cache alpine-sdk

ADD https://gitlab.com/api/v4/projects/4207231/packages/generic/graphviz-releases/$GRAPHVIZ_VERSION/graphviz-$GRAPHVIZ_VERSION.tar.gz /tmp
RUN tar -C /tmp -xzf /tmp/graphviz-$GRAPHVIZ_VERSION.tar.gz
RUN cd /tmp/graphviz-$GRAPHVIZ_VERSION && ./configure && make && make install

RUN apk del --purge -r alpine-sdk && \
		rm -fr /tmp/graphviz-$GRAPHVIZ_VERSION*

ENTRYPOINT ["dot"]
