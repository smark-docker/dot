<!--
SPDX-FileCopyrightText: 2022 Sebastian Mark

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Dockerized DOT

This image provides `dot` from the [graphviz](https://www.graphviz.org/) toolset.

## Usage Example

`docker run -v /tmp/:/tmp/ smark-docker/dot -Tsvg /tmp/foo.json >/tmp/foo.svg`
